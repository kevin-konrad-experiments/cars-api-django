from rest_framework import serializers
from .models import Retailer, Vehicle, Image, Brand


class BrandSerializer(serializers.HyperlinkedModelSerializer):
    vehicles = serializers.StringRelatedField(many=True)

    class Meta:
        model = Brand
        fields = ('url', 'name', 'country', 'vehicles')


class RetailerSerializer(serializers.HyperlinkedModelSerializer):
    latitude = serializers.FloatField(min_value=-90, max_value=90)
    longitude = serializers.FloatField(min_value=-180, max_value=180)
    rate = serializers.FloatField(min_value=0, max_value=5)
    vehicles = serializers.StringRelatedField(many=True)

    class Meta:
        model = Retailer
        fields = ('url', 'name', 'latitude', 'longitude', 'rate', 'vehicles')


class VehicleSerializer(serializers.HyperlinkedModelSerializer):
    year = serializers.IntegerField(min_value=1920, max_value=2040)
    price = serializers.FloatField(min_value=10, max_value=9999999)
    horsepower = serializers.IntegerField(min_value=0, max_value=2000)

    class Meta:
        model = Vehicle
        fields = ('url', 'name', 'year', 'price', 'horsepower', 'retailer', 'brand', 'image')


class ImageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Image
        fields = ('id', 'url', 'description')
